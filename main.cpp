#include <iostream>

using namespace std;

int main() {
    int LENGTH;
    cin >> LENGTH;
    if (LENGTH < 1) {
        cout << "blablabla";
        return -1;
    }
    int data[LENGTH];
    for (int i = 0; i < LENGTH; i++) {
        cin >> data[i];
    }
    char input;
    while (true) {
        cin >> input;
        if (input == 'e') {
            break;
        }
        if (input == 'r') {
            int begin, end, tmp, x1, x2;
            cin >> begin >> end;
            int h = end > begin ? end - begin : LENGTH - begin + end + 1;
            h %= LENGTH;
            for (int i = 0; i < (h + 1) / 2; i++) {
                x1 = (begin + i) % LENGTH;
                x2 = (end - i + LENGTH) % LENGTH;
                tmp = data[x1];
                data[x1] = data[x2];
                data[x2] = tmp;
            }
        } else if (input == 's') {
            int begin, end, k, x1, x2, tmp, id;
            cin >> begin >> end >> k;
            int h = (end > begin ? end - begin : LENGTH - begin + end) + 1;
            h %= LENGTH;
            if (!h) {
                h = 10;
            }
            k = (k + h) % h;
            if (k < 0) {
                k += h;
            }

            for (int i = 0; i < (h + 1) / 2; i++) {
                x1 = (begin + i) % LENGTH;
                x2 = (end - i + LENGTH) % LENGTH;
                tmp = data[x1];
                data[x1] = data[x2];
                data[x2] = tmp;
            }

            for (int i = 0; i < k / 2; i++) {
                id = begin + k - 1;
                x1 = (begin + i) % LENGTH;
                x2 = (id - i + LENGTH) % LENGTH;
                tmp = data[x1];
                data[x1] = data[x2];
                data[x2] = tmp;
            }

            for (int i = 0; i < (h - k) / 2; i++) {
                id = begin + k;
                x1 = (id + i) % LENGTH;
                x2 = (end - i + LENGTH) % LENGTH;
                tmp = data[x1];
                data[x1] = data[x2];
                data[x2] = tmp;
            }
        } else if (input == 'm') {
            int begin, end, k, tmp;
            cin >> begin >> end >> k;
            if (k < 0) {
                for (int i = 0; i >= -(k % LENGTH); i--) {
                    tmp = data[-((begin + i) % LENGTH)];
                    data[-((begin + i) % LENGTH)] = data[-((end + i) % LENGTH)];
                    data[-((end + i) % LENGTH)] = tmp;
                }
            } else {
                for (int i = 0; i <= k % LENGTH; i++) {
                    tmp = data[(begin + i) % LENGTH];
                    data[(begin + i) % LENGTH] = data[(end + i) % LENGTH];
                    data[(end + i) % LENGTH] = tmp;
                }
            }
        }
    }
    for (int i = 0; i < LENGTH; i++) {
        cout << data[i] << " ";
    }
    cout << endl;
    return 0;
}
