cmake_minimum_required(VERSION 3.13)
project(p1_c)

set(CMAKE_CXX_STANDARD 98)

add_executable(p1_c main.cpp)